package com.example.recyclerview.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.arch.core.util.Function;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.example.recyclerview.model.local.db.CafeDataBase;
import com.example.recyclerview.model.local.db.CafeEntity;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DataBaseDisplayViewModel extends AndroidViewModel {
    MutableLiveData<List<CafeEntity>> cafeEntityList;
    MutableLiveData<String> starNum;
    LiveData<List<CafeEntity>> cafeEntityWithStarList;
    ExecutorService ioExecutor;

    public DataBaseDisplayViewModel(@NonNull Application application) {
        super(application);
        ioExecutor = Executors.newSingleThreadExecutor();
        cafeEntityList = new MutableLiveData<>();
        starNum = new MutableLiveData<>();
        cafeEntityWithStarList = Transformations.switchMap(starNum, new Function<String, LiveData<List<CafeEntity>>>() {
            @Override
            public LiveData<List<CafeEntity>> apply(String star) {
                return CafeDataBase.getInstance(getApplication()).getCafeDao().getHighStarLiveList(star);
            }
        });
    }

    public void fetchDataFromDb(String star) {
        try {
            List<CafeEntity> dbList = ioExecutor.submit(new Callable<List<CafeEntity>>() {
                @Override
                public List<CafeEntity> call() throws Exception {
                    return CafeDataBase.getInstance(getApplication()).getCafeDao().getHighStar(star);

                }
            }).get();
            cafeEntityList.postValue(dbList);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setStarNum(String starNum){
        this.starNum.postValue(starNum);
    }

    public LiveData<List<CafeEntity>> getCafeEntityList() {

//        return cafeEntityList;
        return cafeEntityWithStarList;
    }

    public void insertFakeCafe(String star){
        int rand =(int) (Math.random()*1000)+1;
        CafeEntity fakeCafeEntity = new CafeEntity();
        fakeCafeEntity.setName("假咖啡店"+rand);
        fakeCafeEntity.setAddress("某處");
        fakeCafeEntity.setStar(star);
        fakeCafeEntity.setId(String.valueOf(rand));
        insertCafeDataToDb(fakeCafeEntity);

    }

    private void insertCafeDataToDb(CafeEntity cafeEntity) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                CafeDataBase.getInstance(getApplication()).getCafeDao().insert(cafeEntity);
            }
        }).start();
    }
}
