package com.example.recyclerview.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.recyclerview.model.local.db.CafeDataBase;
import com.example.recyclerview.model.local.db.CafeEntity;
import com.example.recyclerview.model.remote.CafeApi;
import com.example.recyclerview.model.remote.CafeData;
import com.example.recyclerview.model.remote.CafeRetrofitClient;
import com.example.recyclerview.view.DisplayData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends AndroidViewModel {
    List<DisplayData> displayDataList ;


    public MainViewModel(@NonNull Application application) {
        super(application);
        displayDataList = new ArrayList<>();

    }

    public void fillDataFromJsonFile(InputStream is) {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is));
        String line = null;
        StringBuilder builder = new StringBuilder();
        try {
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject json = new JSONObject(builder.toString());
            JSONArray dataList = json.getJSONArray("genderData");
            for (int i = 0; i < dataList.length(); i++) {
                JSONObject data = dataList.getJSONObject(i);
                DisplayData displayData = new DisplayData(data.getString("school"),
                        data.getString("sex"),
                        data.getInt("num"));
                displayDataList.add(displayData);
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    public List<DisplayData> getDisplayDataList() {
        return displayDataList;
    }

    public void fetchCafeDataFromApi(String cityName) {
        CafeApi cafeApi = CafeRetrofitClient.getInstance().getCafeApi();
        Call<List<CafeData>> cafeDataList =cafeApi.getCafeList(cityName);
        cafeDataList.enqueue(new Callback<List<CafeData>>() {
            @Override
            public void onResponse(Call<List<CafeData>> call, Response<List<CafeData>> response) {
                List<CafeEntity> cafeEntityList = new ArrayList<>();
                for(CafeData cafeData:response.body()){
                    CafeEntity cafeEntity= new CafeEntity();
                    cafeEntity.setId(cafeData.getId());
                    cafeEntity.setName(cafeData.getName());
                    cafeEntity.setAddress(cafeData.getAddress());
                    cafeEntity.setStar(cafeData.getTasty());
                    cafeEntityList.add(cafeEntity);
                }
                insertCafeDataToDb(cafeEntityList);
            }

            @Override
            public void onFailure(Call<List<CafeData>> call, Throwable t) {

            }
        });
    }

    private void insertCafeDataToDb(List<CafeEntity> cafeEntityList) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                CafeDataBase.getInstance(getApplication()).getCafeDao().insert(cafeEntityList);
            }
        }).start();
    }
}
