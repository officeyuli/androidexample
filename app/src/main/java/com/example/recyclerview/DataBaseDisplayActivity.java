package com.example.recyclerview;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recyclerview.model.local.db.CafeEntity;
import com.example.recyclerview.view.CafeAdapter;
import com.example.recyclerview.viewmodel.DataBaseDisplayViewModel;

import java.util.List;

public class DataBaseDisplayActivity extends AppCompatActivity {
    DataBaseDisplayViewModel viewModel;
    CafeAdapter cafeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);
        viewModel = new ViewModelProvider(this).get(DataBaseDisplayViewModel.class);
        initObserver();
        initView();
        initListener();
    }

    private void initView() {
        RecyclerView cafeRecyclerView = findViewById(R.id.rv_cafe);

        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        cafeRecyclerView.addItemDecoration(divider);

        cafeAdapter = new CafeAdapter();
        cafeRecyclerView.setAdapter(cafeAdapter);
        cafeRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initObserver() {
        viewModel.getCafeEntityList().observe(this, new Observer<List<CafeEntity>>() {
            @Override
            public void onChanged(List<CafeEntity> cafeEntityList) {
                cafeAdapter.setCafeEntityList(cafeEntityList);
                cafeAdapter.notifyDataSetChanged();
            }
        });
    }

    private void initListener() {
        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String starNum = ((TextView) findViewById(R.id.star)).getText().toString();
//                viewModel.fetchDataFromDb(starNum);
                viewModel.setStarNum(starNum);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0); //強制隱藏鍵盤
            }
        });

        findViewById(R.id.fakecafe).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String starNum = ((TextView) findViewById(R.id.star)).getText().toString();
                viewModel.insertFakeCafe(starNum);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });
    }
}
