package com.example.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.recyclerview.model.local.db.CafeDataBase;
import com.example.recyclerview.model.local.db.CafeEntity;
import com.example.recyclerview.model.remote.CafeApi;
import com.example.recyclerview.model.remote.CafeData;
import com.example.recyclerview.model.remote.CafeRetrofitClient;
import com.example.recyclerview.view.DisplayData;
import com.example.recyclerview.view.GenderAdapter;
import com.example.recyclerview.view.OnItemClickListener;
import com.example.recyclerview.viewmodel.MainViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    GenderAdapter adapter;
    MainViewModel viewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        viewModel.fillDataFromJsonFile(getResources().openRawResource(R.raw.gender_data));
        viewModel.fetchCafeDataFromApi("taipei");
        initView();
    }

    private void initView() {
        findViewById(R.id.btn_database).setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, DataBaseDisplayActivity.class);
            startActivity(intent);
        });

        RecyclerView recyclerView = findViewById(R.id.RecyclerView);
        adapter = new GenderAdapter();
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void itemClick(DisplayData displayData) {
                Toast toast =  Toast.makeText(getApplicationContext(),displayData.toString(),Toast.LENGTH_LONG);
                toast.show();
            }
        });
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter.setDisplayDataList(viewModel.getDisplayDataList());
        adapter.notifyDataSetChanged();
    }

}