
package com.example.recyclerview.model.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CafeData {

    @Expose
    private String address;
    @Expose
    private String cheap;
    @Expose
    private String city;
    @Expose
    private String id;
    @Expose
    private String latitude;
    @SerializedName("limited_time")
    private String limitedTime;
    @Expose
    private String longitude;
    @Expose
    private String mrt;
    @Expose
    private String music;
    @Expose
    private String name;
    @SerializedName("open_time")
    private String openTime;
    @Expose
    private String quiet;
    @Expose
    private Double seat;
    @Expose
    private String socket;
    @SerializedName("standing_desk")
    private String standingDesk;
    @Expose
    private String tasty;
    @Expose
    private String url;
    @Expose
    private String wifi;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCheap() {
        return cheap;
    }

    public void setCheap(String cheap) {
        this.cheap = cheap;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLimitedTime() {
        return limitedTime;
    }

    public void setLimitedTime(String limitedTime) {
        this.limitedTime = limitedTime;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMrt() {
        return mrt;
    }

    public void setMrt(String mrt) {
        this.mrt = mrt;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getQuiet() {
        return quiet;
    }

    public void setQuiet(String quiet) {
        this.quiet = quiet;
    }

    public Double getSeat() {
        return seat;
    }

    public void setSeat(Double seat) {
        this.seat = seat;
    }

    public String getSocket() {
        return socket;
    }

    public void setSocket(String socket) {
        this.socket = socket;
    }

    public String getStandingDesk() {
        return standingDesk;
    }

    public void setStandingDesk(String standingDesk) {
        this.standingDesk = standingDesk;
    }

    public String getTasty() {
        return tasty;
    }

    public void setTasty(String tasty) {
        this.tasty = tasty;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWifi() {
        return wifi;
    }

    public void setWifi(String wifi) {
        this.wifi = wifi;
    }

    @Override
    public String toString() {
        return "CafeData{" +
                "address='" + address + '\'' +
                ", cheap='" + cheap + '\'' +
                ", city='" + city + '\'' +
                ", id='" + id + '\'' +
                ", latitude='" + latitude + '\'' +
                ", limitedTime='" + limitedTime + '\'' +
                ", longitude='" + longitude + '\'' +
                ", mrt='" + mrt + '\'' +
                ", music='" + music + '\'' +
                ", name='" + name + '\'' +
                ", openTime='" + openTime + '\'' +
                ", quiet='" + quiet + '\'' +
                ", seat=" + seat +
                ", socket='" + socket + '\'' +
                ", standingDesk='" + standingDesk + '\'' +
                ", tasty='" + tasty + '\'' +
                ", url='" + url + '\'' +
                ", wifi='" + wifi + '\'' +
                '}';
    }
}
