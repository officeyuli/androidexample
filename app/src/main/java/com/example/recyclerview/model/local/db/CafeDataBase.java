package com.example.recyclerview.model.local.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
@Database(entities = {CafeEntity.class},
        version = 1, exportSchema = false)
public abstract class CafeDataBase extends RoomDatabase {
    private static final String DB_NAME = "cafe.db";
    private static volatile CafeDataBase instance;

    public static synchronized CafeDataBase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }

        return instance;
    }

    public static CafeDataBase create(final Context context) {
        return Room.databaseBuilder(
                context,
                CafeDataBase.class,
                DB_NAME)
                .build();
    }

    public abstract CafeDao getCafeDao();

}
