package com.example.recyclerview.model.remote;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CafeApi {
    @GET("/api/v1.2/cafes/{city}")
    Call<List<CafeData>> getCafeList(@Path("city") String city);
}
