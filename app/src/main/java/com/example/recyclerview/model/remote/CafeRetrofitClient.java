package com.example.recyclerview.model.remote;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CafeRetrofitClient {
    private static final String BASE_URL = "https://cafenomad.tw";
    private static CafeRetrofitClient instance;
    private Retrofit retrofit;
    public static synchronized CafeRetrofitClient getInstance() {
        if (instance == null) {
            instance = new CafeRetrofitClient();
        }
        return instance;
    }
    private CafeRetrofitClient() {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.level(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .build();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }
    public CafeApi getCafeApi(){
        return retrofit.create(CafeApi.class);
    }
}
