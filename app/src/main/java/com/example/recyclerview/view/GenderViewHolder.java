package com.example.recyclerview.view;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recyclerview.R;

public class GenderViewHolder extends RecyclerView.ViewHolder {
    OnItemClickListener onItemClickListener;
    View rootView;
    TextView school;
    TextView sex;
    TextView num;
    public GenderViewHolder(@NonNull View itemView) {
        super(itemView);
        rootView = itemView;
        school = itemView.findViewById(R.id.school);
        sex = itemView.findViewById(R.id.sex);
        num = itemView.findViewById(R.id.num);
    }
    public void bind(final DisplayData displayData){
        school.setText(displayData.getSchool());
        sex.setText(displayData.getSex());
        num.setText(String.valueOf(displayData.getCount()));
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onItemClickListener!=null){
                    onItemClickListener.itemClick(displayData);
                }
            }
        });
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
