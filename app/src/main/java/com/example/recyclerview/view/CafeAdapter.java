package com.example.recyclerview.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recyclerview.R;
import com.example.recyclerview.model.local.db.CafeEntity;

import java.util.List;

public class CafeAdapter extends RecyclerView.Adapter<CafeViewHolder> {
    List<CafeEntity> cafeEntityList;
    @NonNull
    @Override
    public CafeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cafe, parent, false);
        return new CafeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CafeViewHolder holder, int position) {
        CafeEntity cafeEntity = cafeEntityList.get(position);
        if(cafeEntity!=null){
            holder.bind(cafeEntity);
        }
    }

    @Override
    public int getItemCount() {
        return cafeEntityList==null?0:cafeEntityList.size();
    }

    public void setCafeEntityList(List<CafeEntity> cafeEntityList){
        this.cafeEntityList = cafeEntityList;
    }
}
