package com.example.recyclerview.view;

public class DisplayData {
    private String school;
    private String sex;
    private int count;

    public DisplayData(String school, String sex, int count) {
        this.school = school;
        this.sex = sex;
        this.count = count;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "DisplayData{" +
                "school='" + school + '\'' +
                ", sex='" + sex + '\'' +
                ", count=" + count +
                '}';
    }
}
