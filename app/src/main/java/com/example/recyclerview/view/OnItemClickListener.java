package com.example.recyclerview.view;

public interface OnItemClickListener {
    void itemClick(DisplayData displayData);
}
