package com.example.recyclerview.view;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recyclerview.R;
import com.example.recyclerview.model.local.db.CafeEntity;


public class CafeViewHolder extends RecyclerView.ViewHolder {
    TextView name;
    TextView address;
    TextView star;
    public CafeViewHolder(@NonNull View itemView) {
        super(itemView);
        name = itemView.findViewById(R.id.tv_name);
        address = itemView.findViewById(R.id.tv_address);
        star = itemView.findViewById(R.id.tv_star);
    }
    public void bind(CafeEntity cafeEntity){
        name.setText(cafeEntity.getName());
        address.setText(cafeEntity.getAddress());
        star.setText(cafeEntity.getStar());
    }
}
