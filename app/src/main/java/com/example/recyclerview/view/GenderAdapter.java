package com.example.recyclerview.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recyclerview.R;

import java.util.List;

public class GenderAdapter extends RecyclerView.Adapter<GenderViewHolder> {
    List<DisplayData> displayDataList;
    OnItemClickListener onItemClickListener;
    @NonNull
    @Override
    public GenderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if(viewType==0){
            view= LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_female, parent, false);
        }else{
            view= LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_male, parent, false);
        }

        return new GenderViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull GenderViewHolder holder, int position) {
        DisplayData item = displayDataList.get(position);
        if (item != null) {
            holder.bind(item);
            if (onItemClickListener != null) {
                holder.setOnItemClickListener(onItemClickListener);
            }
        }
    }

    @Override
    public int getItemCount() {
        return displayDataList!=null?displayDataList.size():0;
    }

    public void setDisplayDataList(List<DisplayData> displayDataList) {
        this.displayDataList = displayDataList;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        DisplayData item = displayDataList.get(position);
        if(item!=null){
            if(item.getSex().equals("男")){
                return 1;
            }else{
                return 0;
            }
        }
        return super.getItemViewType(position);
    }
}
